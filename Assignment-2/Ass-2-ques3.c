#include<stdio.h>
int main()
{
	// program to check a given character is Vowel or Consonant.
	char letter;
	
	printf("Enter a letter : ");
	scanf("%c", &letter);

    //To condition to check character is alphabet or not 
    if((letter >='a' && letter <= 'z') || (letter >='A' && letter <= 'Z'))
    {
    	//To check whether vowel or consonant
    	switch(letter)
    	{
    		case 'a':
    		case 'e':
			case 'i':
			case 'o':
			case 'u':
			case 'A':
			case 'E':
			case 'I':
			case 'O':
			case 'U':
				printf("%c is a vowel",letter);
				break;
			default :
				printf("%c is a consonant",letter);
				
		}
    }
	else
		printf("Not an Alphabet");
    		
	return 0;
}

